<?php

class SiteController extends Controller
{
	public function actionIndex()
	{
            $meta = MetaTags::model()->findByPk(1);
            $criteria = new CDbCriteria();
            $criteria->order = '`id` ASC';
            $review = Reviews::model()->findAll($criteria);
            if ( isset($_POST['ContactForm']) ) {
                $model = new ContactForm();
               
                $model->attributes = $_POST['ContactForm'];
                if ( $model->validate() ) {
                    $model->goContact();
                    Yii::app()->user->setFlash('success', true);
                    $model->date = time();
                    $model->ip = Yii::app()->request->userHostAddress;
                    $model->save(false);
                }
                //Yii::app()->end();
            }
            $this->render('index', array(
                'meta' => $meta,
                'review' => $review,
            ));
        }
        
        public function actionTest()
	{
            $this->render('test');
	}
        
        public function actionError()
	{
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
	}
       
        public function actionContact()
        {
            $model = new ContactForm();

            if ( isset($_POST['ajax']) && $_POST['ajax']==='contact-form' ) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if ( isset($_POST['ContactForm']) ) {
                $model->attributes = $_POST['ContactForm'];
                if ( $model->validate() ) {
                    Yii::app()->user->setFlash('success', true);
                    $model->goContact(); 
                }
                //Yii::app()->end();
            }
            $this->redirect(array('site/index'));
        }
        
}