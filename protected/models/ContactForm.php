<?php

/**
 * This is the model class for table "contactForm".
 *
 * The followings are the available columns in table 'contactForm':
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property integer $date
 */
class ContactForm extends CActiveRecord
{
       // public $name;
       // public $phone;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contactForm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, phone', 'required'),
			array('date', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>64),
			array('phone', 'length', 'max'=>32),
                        array('ip', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ip, name, phone, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
                        'ip' => 'ip',
			'name' => 'Контактное лицо',
			'phone' => 'Телефон',
			'date' => 'Дата',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('id',$this->id);
                $criteria->compare('ip',$this->ip,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		//$criteria->compare('date',$this->date);

		$dataProvider = new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                        'pageSize'=>20,
                    ),
                ));

                $dataProvider->sort->defaultOrder = '`id` DESC';

                return $dataProvider;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContactForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function goContact()
        {   
            include_once "libmail.php";
            $m = new Mail;
            //$m->From($this->email.';admin@google.com');
            $m->smtp_on('ssl://smtp.gmail.com', 'i525643@gmail.com', '3W84R3GWxty', '465');
            $m->From('macadam.ru'.';i525643@gmail.com');
            $m->To(array(Yii::app()->controller->config->adminEmail));
            $m->Subject('Тема письма');
            $m->Body('Отправитель: '.$this->name.' Телефон: '.$this->phone);
            $m->Send();
        }
}
