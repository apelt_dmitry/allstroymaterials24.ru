<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    
    // bootstrap 3.3.1
    ->registerCssFile($pt.'css/bootstrap.min.css')
    // bootstrap theme
    ->registerCssFile($pt.'css/bootstrap-theme.min.css')
    //preLoader
    ->registerCssFile($pt.'css/preLoader.css')
    //animate.css http://daneden.github.io/animate.css/
    ->registerCssFile($pt.'css/animate.css')
    //magic.css http://www.minimamente.com/example/magic_animations/
    ->registerCssFile($pt.'css/magic.css')
    // carousel_middle
    ->registerCssFile($pt.'css/carousel_middle.css')
        
    ->registerCssFile($pt.'css/main.css');

$cs
    
    ->registerCoreScript('jquery',CClientScript::POS_END)
    ->registerCoreScript('jquery.ui',CClientScript::POS_END)
    ->registerCoreScript('cookie',CClientScript::POS_END)
    //->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/jquery-1.8.2.min.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/jquery.carouFredSel-6.0.4-packed.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/sliders.js',CClientScript::POS_END)
        
    ->registerScriptFile($pt.'js/main.js',CClientScript::POS_END);

    $config=$this->config;
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?=Yii::app()->name; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="ru">
        <meta name="robots" content="index, follow">
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/png">
    </head>
    <body>
        <script>
            // прелоадер начало
            $(document).ready(function () {
                $('#loader-wrapper').animate({
                    opacity: 0
                }, 2000, null, function () {
                    $(this).hide();
                });
            });
            // прелоадер конец 
        </script>
        
        <div id="loader-wrapper">
            <div id="loader"></div>
        </div>
        <div class="header">
            <div class="container">
                <div class="header_arenda">Песок, щебень с доставкой<br/><span>в Омске</span></div>
                <div class="header_number_box">
                    <img class="header_arrow" src="/img/head_arrow.png"/>
                    <div class="header_call">Есть вопросы? Звоните!</div>
                    <div class="header_number"><span class="lptracker_phone">8-904-328-61-57</span></div>
                </div>
            </div>
        </div>
        
        <?= $content ?>
        <!-- Модальное окно подтверждения отправки -->
        <a href="#win5"  id="click_me"></a>
        <a href="#x" class="overlay" id="win5"></a>
        <div class="popup">
            <img class="is-image" src="/img/overlay_modal.png" alt="" />
            <a class="close" title="Закрыть" href="#close"></a>
        </div>
        <div style="display: none;">
            <?php 
                $yandex = Config::model()->findByPk(1);
                echo $yandex->yandex; 
            ?>
        </div>
        
        <div class="footer">
            <div class="container">
                <div class="header_arenda">Песок, щебень с доставкой<br/><span>в Омске</span></div>
                <div class="header_number_box">
                    <img class="header_arrow" src="/img/head_arrow.png"/>
                    <div class="header_call">Есть вопросы? Звоните!</div>
                    <div class="header_number"><span class="lptracker_phone">8-904-328-61-57</span></div>
                </div>
            </div>
        </div>
    </body>
</html>