<?php
Yii::app()->name = $meta->meta_title;
Yii::app()->clientScript->registerMetaTag($meta->meta_description, 'description');
Yii::app()->clientScript->registerMetaTag($meta->meta_keywords, 'keywords');
?>

<?php if ( Yii::app()->user->getFlash('success') ): ?>
<script>
    $(document).ready(function(){
        var loc = location;
        location =  loc + '#win5';
    });
</script>
<?php endif; ?>
        <div class="after_header">
            <div class="after_header_bgr"></div>
            <div class="container">                
                <div class="form_zakaz_box">
                    <div class="form_zakaz_text_top">Оставьте заявку</div>
                    <div class="form_zakaz_text_bot">и мы Вам перезвоним</div>
                    <?php 
                        $contactForm = new ContactForm();
                        $form = $this->beginWidget('CActiveForm', array(
                            'id'=>'contact-form',
                            'action'=>array('site/index'),
                            'enableAjaxValidation'=>true,
                            'enableClientValidation'=>true,
                            'clientOptions'=>array( 
                                'validateOnChange'=>false,
                                'validateOnSubmit'=>true,
                            ),
                    )); ?>

                    <?php echo $form->errorSummary($contactForm); ?>

                    <div class="row form_zakaz_name">
                        <?php echo $form->textField($contactForm,'name',array('placeHolder' => 'Ваше имя', 'required' => 'required')); ?>
                    </div>
                    <div class="row form_zakaz_phone">
                        <?php echo $form->textField($contactForm,'phone',array('placeHolder' => 'Ваш телефон', 'required' => 'required')); ?>
                        
                    </div>
                    <div class="row">
                        <?= CHtml::submitButton('', array(
                            'class'=>'button_zakaz',
                        )) ?>
                    </div>

                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="cube_box">
                <div class="cube_left"></div><div class="zakaz_u_nas">У нас вы можете заказать</div><div class="cube_right"></div>
            </div>
            
            <div class="sand_box">
                <span class="secret"></span>
                <img src="/img/sand_pesok.jpg" class="sand_img"/>
                <img src="/img/sand_cube.png" class="sand_cube"/>
                <p class="sand_text">песок</p>
                <p class="sand_text_under">250 руб / т</p>
            </div>
            <div class="sand_box">
                <span class="secret"></span>
                <img src="/img/sand_suspes.jpg" class="sand_img"/>
                <img src="/img/sand_cube.png" class="sand_cube"/>
                <p class="sand_text">Супесь</p>
                <p class="sand_text_under">200 руб / т</p>
            </div>
            <div class="sand_box">
                <span class="secret"></span>
                <img src="/img/sand_sheben_5_20.jpg" class="sand_img"/>
                <img src="/img/sand_cube.png" class="sand_cube"/>
                <p class="sand_text">щебень фракция 5-20</p>
                <p class="sand_text_under">900 руб / т</p>
            </div>
            <div class="sand_box">
                <span class="secret"></span>
                <img src="/img/sand_sheben_20_40.jpg" class="sand_img"/>
                <img src="/img/sand_cube.png" class="sand_cube"/>
                <p class="sand_text">щебень фракция 20-40</p>
                <p class="sand_text_under">880 руб / т</p>
            </div>
            <div class="sand_box dont_do_this_again">
                <span class="secret"></span>
                <img src="/img/sand_sheben_40_70.jpg" class="sand_img"/>
                <img src="/img/sand_cube.png" class="sand_cube"/>
                <p class="sand_text">щебень фракция 40-70</p>
                <p class="sand_text_under">880 руб / т</p>
            </div>
        </div>
        <div class="orange_line">
            <div class="container">
                <div class="prem_box">
                    <img src="/img/prem_1.png" class="prem_img"/>
                    <div class="prem_text">
                        Высокое качество
                    </div>
                </div>
                <div class="prem_box">
                    <img src="/img/prem_2.png" class="prem_img"/>
                    <div class="prem_text">
                        Доставка в другие города
                    </div>
                </div>
                <div class="prem_box">
                    <img src="/img/prem_3.png" class="prem_img"/>
                    <div class="prem_text">
                        Гарантия сроков
                    </div>
                </div>
                <div class="prem_box">
                    <img src="/img/prem_4.png" class="prem_img"/>
                    <div class="prem_text">
                        Вся продукция сертифицирована
                    </div>
                </div>
            </div>
        </div>
        <div class="call_box">
            <div class="container">
                <div class="form_zakaz_box_2">
                    <div class="form_zakaz_text_top_2">Оставьте заявку</div>
                    <div class="form_zakaz_text_bot_2">и мы Вам перезвоним</div>
                    <?php 
                        $contactForm = new ContactForm();
                        $form = $this->beginWidget('CActiveForm', array(
                            'id'=>'contact-form',
                            'action'=>array('site/index'),
                            'enableAjaxValidation'=>true,
                            'enableClientValidation'=>true,
                            'clientOptions'=>array( 
                                'validateOnChange'=>false,
                                'validateOnSubmit'=>true,
                            ),
                    )); ?>
                    <?php echo $form->errorSummary($contactForm); ?>
                    <div class="form_box">
                        <div class="form_zakaz_name_2">
                            <?php echo $form->textField($contactForm,'name',array('placeHolder' => 'Ваше имя', 'required' => 'required')); ?>
                        </div>
                        <div class="form_zakaz_phone_2">
                            <?php echo $form->textField($contactForm,'phone',array('placeHolder' => 'Ваш телефон', 'required' => 'required')); ?>
                        </div>

                        <?= CHtml::submitButton('', array(
                            'class'=>'button_zakaz_2',
                        )) ?>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
        
        <div class="spec_slider_bgr">
            <div class="container">
                <div class="cube_box">
                    <div class="cube_left"></div><div class="zakaz_u_nas">также вы можете заказать</div><div class="cube_right"></div>
                </div>
            </div>
            <div id="wrapper_glav">
                <div id="inner_glav">
                    <div id="carousel_glav">
                        <div class="glavnaja_block">
                            <p class="glavnaja_block_title">АВТОВЫШКИ</p>
                            <img src="/img/glavnaja_avtovishka.png"/>
                            <a target="_blank" href="http://allspectehnika24.ru/avtovishki" class="glavnaja_block_button"></a>
                        </div>
                        <div class="glavnaja_block">
                            <p class="glavnaja_block_title">АВТОКРАН</p>
                            <img id="glavnaja_avtokran_img"src="/img/glavnaja_avtokran.png"/>
                            <a target="_blank" href="http://allspectehnika24.ru/avtokran" class="glavnaja_block_button"></a>
                        </div>
                        <div class="glavnaja_block">
                            <p class="glavnaja_block_title">фронтальный погрузчик</p>
                            <img src="/img/glavnaja_pogruzchik.png"/>
                            <a target="_blank" href="http://allspectehnika24.ru/pogruzchik" class="glavnaja_block_button"></a>
                        </div>
                        <div class="glavnaja_block">
                            <p class="glavnaja_block_title">Длинномер</p>
                            <img  id="glavnaja_dlinnomer_img" src="/img/glavnaja_dlinnomer.png"/>
                            <a target="_blank" href="http://allspectehnika24.ru/dlinnomer" class="glavnaja_block_button"></a>
                        </div>
                    </div>
                    <a href="#" id="prev_glav"></a>
                    <a href="#" id="next_glav"></a>
                </div>
            </div>
        </div>
        
        <div class="reviews_box">
            <div class="reviews_bgr"></div>
            <div class="container">
                <div class="cube_box_reviews">
                    <div class="cube_left_reviews"></div><div class="zakaz_u_nas_reviews">отзывы</div><div class="cube_right"></div>
                </div> 
                <?php foreach ($review as $row):?>
                <div class="reviews">
                    <div class="avatarKaina">
                        <img src="/img/white_triangle.png" class="white_triangle"/>
                        <img src="/uploads/reviews/preview/<?= $row->preview ?>" class="img_avatarKaina"/>
                    </div>
                    <div class="white_box">
                        <div class="white_box_text"><?= $row->review ?></div>
                        <div class="white_box_name"><?= $row->name ?></div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="reviews_space"></div>
            <div class="call_box_reviews">
                <div class="container">
                    <div class="form_zakaz_box_3">
                        <div class="form_zakaz_text_top_2">Оставьте заявку</div>
                        <div class="form_zakaz_text_bot_2">и мы Вам перезвоним</div>
                        <?php 
                            $contactForm = new ContactForm();
                            $form = $this->beginWidget('CActiveForm', array(
                                'id'=>'contact-form',
                                'action'=>array('site/index'),
                                'enableAjaxValidation'=>true,
                                'enableClientValidation'=>true,
                                'clientOptions'=>array( 
                                    'validateOnChange'=>false,
                                    'validateOnSubmit'=>true,
                                ),
                        )); ?>
                        <?php echo $form->errorSummary($contactForm); ?>
                        <div class="form_box">
                            <div class="form_zakaz_name_2">
                                <?php echo $form->textField($contactForm,'name',array('placeHolder' => 'Ваше имя', 'required' => 'required')); ?>
                            </div>
                            <div class="form_zakaz_phone_2">
                                <?php echo $form->textField($contactForm,'phone',array('placeHolder' => 'Ваш телефон', 'required' => 'required')); ?>
                            </div>

                            <?= CHtml::submitButton('', array(
                                'class'=>'button_zakaz_2',
                            )) ?>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        </div>
    
       
