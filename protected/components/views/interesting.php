<?php
    $criteria = new CDbCriteria();
    $criteria->select = array('id', 'preview', 'title', 'description');
    $criteria->order = '`id` DESC';
    $interesting = Interesting::model()->findAll($criteria);
?>
<div class="books">
    <?php 
        mb_internal_encoding("UTF-8");
        foreach ( $interesting as $row ): 
    ?>
    <div class="book item_1">
        <a href="#"><img src="/uploads/interesting/preview/<?= $row->preview; ?>"></a>
        <p class="title"><a href="#"><?= $row->title; ?></a></p>
        <div class="short">
            <p><span><?= $row->description; ?></span></p>
        </div>
        <div class="clear"></div>
    </div>
    <?php endforeach; ?>
</div>
<div class="clear"></div>
