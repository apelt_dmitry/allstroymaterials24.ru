<?php

    $cs = Yii::app()->clientScript;
    $pt = Yii::app()->homeUrl;

    $cs
        // bootstrap 3.3.1
        ->registerCssFile($pt.'css/bootstrap.min.css')
        ->registerCssFile($pt.'css/admin.css')
        // bootstrap theme
        ->registerCssFile($pt.'css/bootstrap-theme.min.css');
    
    $cs
        ->registerCoreScript('jquery',CClientScript::POS_END)
        ->registerCoreScript('jquery.ui',CClientScript::POS_END)
        ->registerCoreScript('cookie',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
        ->registerScriptFile($pt.'js/admin.js',CClientScript::POS_END);

    //$config = $this->config;

?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <!--link rel="icon" type="image/png" href="/favicon.png" /-->

    <title><?= CHtml::encode($this->pageTitle); ?></title>

</head>

<body>
    
    <div class="container">
        
        <div class="row">
            
            <div id="header">
                <?php
                    $this->widget('bootstrap.widgets.BsNavbar', array(
                        'brandLabel' => BsHtml::icon(BsHtml::GLYPHICON_HOME),
                        'brandUrl' => Yii::app()->homeUrl,
                        'brandOptions'=>array(
                            'target'=> '_blank'
                        ),
                        'items' => array(
                            /*array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Конфигурация системы',
                                        'url' => array('/admin/default/index'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                ),
                            ),*/


                            array(
                            'class' => 'bootstrap.widgets.BsNav',
                            'encodeLabel' => false,
                            'type' => 'navbar',
                            'activateParents' => true,
                            'items' => array(
                                array(
                                    'label' => 'Заявки',
                                    'url' => array('/admin/contactForm/index'),
                                    'visible'=>!Yii::app()->user->isGuest,
                                    )
                            )),
                            
                            array(
                            'class' => 'bootstrap.widgets.BsNav',
                            'encodeLabel' => false,
                            'type' => 'navbar',
                            'activateParents' => true,
                            'items' => array(
                                array(
                                    'label' => 'Мета теги',
                                    'url' => array('/admin/metaTags/index'),
                                    'visible'=>!Yii::app()->user->isGuest,
                                    )
                            )),
                            
                            array(
                            'class' => 'bootstrap.widgets.BsNav',
                            'encodeLabel' => false,
                            'type' => 'navbar',
                            'activateParents' => true,
                            'items' => array(
                                array(
                                    'label' => 'Отзывы',
                                    'url' => array('/admin/reviews/index'),
                                    'visible'=>!Yii::app()->user->isGuest,
                                    )
                            )),
                            
                            
                            array(
                            'class' => 'bootstrap.widgets.BsNav',
                            'encodeLabel' => false,
                            'type' => 'navbar',
                            'activateParents' => true,
                            'items' => array(
                                array(
                                    'label' => 'Конфигурация',
                                    'url' => array('/admin/config/index'),
                                    'visible'=>!Yii::app()->user->isGuest,
                                    )
                            )),
     

                            /*array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'encodeLabel' => false,
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Содержимое страниц / управление меню',
                                        'url' => array('/admin/menu/index'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                    array(
                                        'label' => 'Посетители',
                                        'url' => 'https://metrika.yandex.ru/stat/?counter_id=28184046',
                                        'linkOptions'=>array(
                                            'target'=>'_blank',
                                        ),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                ),
                            ),*/
                            array(
                                'class' => 'bootstrap.widgets.BsNav',
                                'type' => 'navbar',
                                'activateParents' => true,
                                'items' => array(
                                    array(
                                        'label' => 'Вход',
                                        'url' => array('/admin/default/login'),
                                        'visible'=>Yii::app()->user->isGuest,
                                    ),
                                    array(
                                        'label' => 'Выход',
                                        'url' => array('/admin/default/logout'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                ),

                                'htmlOptions' => array(
                                    'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT,
                                    'style' => 'margin-right: 10px;',
                                ),
                            ),

                        ),
                    ));
                ?>
            </div>
                
            <!--div id="specialPanel">

            </div-->

            <div id="content">
                <?= $content ?>
            </div>



        </div>

        <div id="footer" class="panel panel-footer panel-default row">
            <div id="timeInfo" class="col_l col-md-6"><small>Время сервера: <?= date('d.m.Y, H:i') ?>.</small></div>
            <div id="logInfo" class="col_r col-md-6 text-right"><small>Страница сгенерирована за: <?=round(Yii::getLogger()->executionTime, 3).' сек' ?>.</small></div>
        </div>
        
    </div>
    
</body>
</html>