<?php echo BsHtml::pageHeader('Отзывы') ?>


<?= BsHtml::linkButton('Добавить  отзыв', array(
    'icon' => BsHtml::GLYPHICON_PLUS,
    'color' => BsHtml::BUTTON_COLOR_SUCCESS,
    'url' => array('/admin/reviews/create'),
    //'target'=>'_blank',
    //'block' => true,
    'style'=>'float: right;margin-right:10px;',
)); ?>

<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>null,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
       
        array(
            'header' => 'preview',
            'type'=>'raw',
            'value' => 'BsHtml::image("/uploads/reviews/preview/".$data->preview)',
        ),
        array(
            'name'=>'name',
        ),
        array(
            'name'=>'review',
            'type'=>'raw',
        ),
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn'
        ),
    ),
)); ?>