<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'menu-form',
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnChange'=>false,
        'validateOnSubmit'=>true,
    ),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'meta_title',array('maxlength'=>32)); ?>

    <?= $form->textAreaControlGroup($model,'meta_keywords', array('rows' => 10)); ?>

    <?= $form->textAreaControlGroup($model,'meta_description', array('rows' => 10)); ?>
    


    <?= BsHtml::formActions(array(
        BsHtml::resetButton('Сброс', array(
            'color' => BsHtml::BUTTON_COLOR_WARNING,
            'icon' => BsHtml::GLYPHICON_REFRESH,
        )),
        BsHtml::submitButton('Готово', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
    ), array('class'=>'form-actions')); ?>

<?php $this->endWidget(); ?>
