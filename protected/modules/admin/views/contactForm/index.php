<?php echo BsHtml::pageHeader('Заявки') ?>

<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
       
        array(
            'name' => 'date',
            'value' => 'Yii::app()->dateFormatter->format(\'HH:mm d MMM yyyy г.\', $data->date)',
            'filter' => false,
        ),
        array(
            'name'=>'ip',
        ),
        array(
            'name'=>'name',
        ),
        array(
            'name'=>'phone',
        ),
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            'template' => '{delete}'
        ),
    ),
)); ?>