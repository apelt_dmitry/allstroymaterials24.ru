<?php
    $this->pageTitle = Yii::app()->name.' - '.'Конфигурация системы';
?>
<?php $this->beginWidget('bootstrap.widgets.BsPanel', array(
    'title' => 'Конфигурация системы',
)); ?>

    <?php if ( Yii::app()->user->getFlash('success') ): ?>
        <?= BsHtml::alert(BsHtml::ALERT_COLOR_INFO, 'Информация обновлена.') ?>
    <?php endif; ?>

    <?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id'=>'config-form',
        'enableAjaxValidation'=>false,
        'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnChange'=>true,
            'validateOnSubmit'=>true,
        ),
    )); ?>

        <?= $form->errorSummary($model); ?>

        <?= $form->textFieldControlGroup($model,'adminEmail',array(
            'maxlength'=>64,
        )); ?>

        <?= $form->textAreaControlGroup($model,'yandex', array('rows' => 10)); ?>

        <div class="panel panel-danger">
            <div class="panel-heading">Изменение пароля</div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="User_password">Старый пароль</label>
                    <div class="col-lg-10"><input maxlength="16" name="oldPassword" class="form-control" placeholder="Старый пароль" type="text" value=""></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2" for="User_password">Новый пароль</label>
                    <div class="col-lg-10"><input maxlength="16" name="newPassword" class="form-control" placeholder="Новый пароль" type="text" value=""></div>
                </div>
            </div>
        </div>

        <?= BsHtml::formActions(array(
            BsHtml::resetButton('Сброс', array(
                'color' => BsHtml::BUTTON_COLOR_WARNING,
                'icon' => BsHtml::GLYPHICON_REFRESH,
            )),
            BsHtml::submitButton('Готово', array(
                'color' => BsHtml::BUTTON_COLOR_SUCCESS,
                'icon' => BsHtml::GLYPHICON_OK,
            )),
        ), array('class'=>'form-actions')); ?>

    <?php $this->endWidget(); ?>

<?php $this->endWidget(); ?>