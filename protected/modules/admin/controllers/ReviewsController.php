<?php

class ReviewsController extends AdminController
{

    
    public function actionCreate()
    {
        $model = new Reviews();
        
        if ( isset($_POST['Reviews']) ) {
            $model->attributes = $_POST['Reviews'];
            $model->image=CUploadedFile::getInstance($model,'image');
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/reviews/'.$imageName);
                    $image->resize(171, 171);
                    $image->save('./uploads/reviews/preview/'.$imageName);
                    $model->preview = $imageName;
                }
                if($model->save(FALSE)){
                    
                    $this->redirect(array('index'));
                }
            }
        }
        $this->render('create', array(
            'model'=>$model,
        ));
    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        if ( isset($_POST['Reviews']) ) {
            $model->attributes = $_POST['Reviews'];
            $model->image=CUploadedFile::getInstance($model,'image');
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/reviews/'.$imageName);
                    $image->resize(171, 171);
                    $image->save('./uploads/reviews/preview/'.$imageName);
                    $model->preview = $imageName;
                }
                if($model->save(FALSE)){

                    $this->redirect(array('index'));
                }
            }
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Reviews('search');
        $model->unsetAttributes();
        if ( isset($_GET['Reviews']) ) {
            $model->attributes=$_GET['Reviews'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Reviews::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}

