<?php

class ContactFormController extends AdminController
{
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function actionIndex()
    {
        $model = new ContactForm('search');
        $model->unsetAttributes();
        if ( isset($_GET['ContactForm']) ) {
            $model->attributes=$_GET['ContactForm'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }

    public function loadModel($id)
    {
        $model=ContactForm::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}
