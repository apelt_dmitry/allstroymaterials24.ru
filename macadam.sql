-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 06 2015 г., 15:03
-- Версия сервера: 5.5.38-log
-- Версия PHP: 5.5.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `macadam`
--

-- --------------------------------------------------------

--
-- Структура таблицы `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminEmail` varchar(64) NOT NULL,
  `yandex` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `config`
--

INSERT INTO `config` (`id`, `adminEmail`, `yandex`) VALUES
(1, 'mitridat115@yandex.ru', '--');

-- --------------------------------------------------------

--
-- Структура таблицы `contactForm`
--

CREATE TABLE IF NOT EXISTS `contactForm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `date` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Структура таблицы `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `info`
--

INSERT INTO `info` (`id`, `description`) VALUES
(1, '<p><span style="font-size:14px"><span style="color:#FF0000"><u>Я приобрел прогноз и ставка не сыграла &ndash; что делать?</u></span></span></p>\r\n\r\n<p><span style="font-size:14px">Главное &ndash; не волноваться! Проигрыши &ndash; это часть нашей профессии. Проигрыши делают нас сильнее, мудрее, закаляют нас в профессиональном плане. Важно помнить, что заработок на ставках &ndash; это работа на дистанцию. Старайтесь ставить ежедневно не более 10% от банка на наши прогнозы &ndash; и по итогам месяца вы будете в хорошем плюсе. Просто взгляните на страницу статистики и убедитесь в этом сами!</span></p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `main`
--

CREATE TABLE IF NOT EXISTS `main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `main`
--

INSERT INTO `main` (`id`, `name`, `body`) VALUES
(1, 'Главная', '<p>123</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `z_index` int(11) NOT NULL,
  `body` text NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `name`, `z_index`, `body`, `parent_id`) VALUES
(1, 'Главная', 0, '<p style="margin-left:20px; text-align:justify"><img alt="" src="/uploads/ckeditor/f499345f146140d4e2e735cab414.JPG" style="float:left; height:400px; margin:7px 7px 7px 0px; width:267px" /></p>\r\n\r\n<p style="text-align:justify">Человек всегда хотел заглянуть в свое будущее.&nbsp;<br />\r\nВо всех мировых культурах можно найти&nbsp; различные виды<strong>предсказаний</strong>.&nbsp;<br />\r\nИх цель &ndash;&nbsp; увидеть&nbsp; грядущие события и ситуации, чтобы потом по возможности избежать многих неприятностей и хотя бы слегка обеспечить благополучие и стабильность в жизни.</p>\r\n\r\n<p style="text-align:justify"><strong>Гадание</strong>&nbsp;было&nbsp; значительным элементом всех древних верований и религий. Есть свидетельства, что в течение сотен лет предсказания на<strong>картах Таро</strong>&nbsp;были востребованным средством получения информации по интересующим людей вопросам.</p>\r\n\r\n<p style="text-align:justify">Многие виды искусства&nbsp;<strong>предсказания будущего</strong>&nbsp;передавались из одного&nbsp; поколения в другое. Наиболее известна такая предсказательная система, как&nbsp;<strong>карты Таро</strong>.</p>\r\n\r\n<p style="text-align:justify">Каждый из нас, наверное, хотя бы раз слышал о магических картах Таро. Возникновение карт окутано тайной, никто точно не знает, откуда они взялись, существуют только догадки и красивые легенды.</p>\r\n\r\n<p style="text-align:justify">Что можно узнать с помощью карт Таро, на какие вопросы получить ответы? Да практически на любой вопрос карты Таро могут дать ответ, посоветовать, как поступить в той или иной ситуации.</p>\r\n\r\n<p style="text-align:justify">Конечно, наиболее всего людей волнуют вопросы&nbsp;<strong>взаимоотношений</strong>&nbsp;с другим человеком, стоит ли с ним дальше строить отношения, чего от него можно ожидать, не завел ли он (она) себе другую. А&nbsp; может быть&nbsp; вообще не стоит даже начинать эти отношения, потому что ничего, кроме боли и обид они не принесут?</p>\r\n\r\n<p style="text-align:justify">Кстати, мне задают вопрос, можно ли&nbsp;<strong>гадать</strong>&nbsp;на иностранца. Конечно, можно. Производить гадание можно на человека любой национальности, вероисповедания и живущего в любом уголке земли. Для карт это не имеет значения.</p>\r\n\r\n<p style="text-align:justify">Ответят карты Таро и на вопросы, связанные с бизнесом: чего можно ожидать при его открытии, как будут вести себя конкуренты, чего опасаться, как будет развиваться ситуация в дальнейшем.</p>\r\n\r\n<p style="text-align:justify">С помощью карт Таро можно четко увидеть, какое на человеке имеется<strong>магическое воздействие</strong>: порча, сглазы, привороты, проклятья, подселенцы, импланты, в каком состоянии находится его защита и энергетика.&nbsp;</p>\r\n\r\n<p style="text-align:justify">Карты посоветуют, что нужно делать в этом случае, как избавиться от этого &laquo;добра&raquo;, которое на вас повесили &laquo;добрые люди&raquo;.</p>\r\n\r\n<p style="text-align:justify">Вы можете получить ответ и о состоянии здоровья, но этот вид гадания<strong>тарологи</strong>&nbsp;выполняют неохотно. Оно и понятно: это большая ответственность. А умный таролог должен работать, как и врач, по принципу: &laquo;Не навреди!&raquo;&nbsp;<br />\r\nНо если человеку предстоит какое-то серьезное лечение или операция, конечно, ему хочется знать, будет ли от этого толк.</p>\r\n\r\n<p style="text-align:justify">Я практикую гадание на картах Таро уже много лет, достигла определённого уровня, что позволяет мне профессионально оказывать услуги по гаданию и предсказанию на Таро тем, кто ко мне обращается.<br />\r\nПрактикую как личный прием, так и гадание по телефону, по фотографии - то есть удалённое гадание без присутствия гадающего.</p>\r\n\r\n<p style="text-align:justify">Удалённое гадание ничем не хуже, чем если бы вы сидели рядом со мной и видели сам процесс. Иногда это даже лучше, потому что гадающий не влияет своими мыслями, эмоциями, желаниями на карты, а значит и на результат более достоверный.</p>\r\n\r\n<p style="text-align:justify">Система Таро способна помочь в решении многих жизненно важных вопросов, а я , таролог Галина Мирова, постараюсь&nbsp; помочь&nbsp; вам в этом.</p>\r\n', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `meta_tags`
--

CREATE TABLE IF NOT EXISTS `meta_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_keywords` text,
  `meta_title` text,
  `meta_description` text,
  `name_page` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `meta_tags`
--

INSERT INTO `meta_tags` (`id`, `meta_keywords`, `meta_title`, `meta_description`, `name_page`) VALUES
(1, 'Щебень', 'Щебень', 'Щебень', 'Песок. Щебень.');

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preview` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `review` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `preview`, `name`, `review`) VALUES
(1, '5202578a3c5332c13bdbd7eddae4.png', 'Ливанов Э. В., ИП', '<p>В эту компанию обратились недавно, до этого времени закупали нерудные материалы в других компаниях. Многие наши бывшие партнеры подняли цены, мы стали искать приемлемую альтернативу. В итоге мы значительно выиграли и в цене, и в качестве, и в сервисе. Надеюсь, что так будет и впредь.</p>\r\n'),
(2, '31212874b059a50636bc7ce2fcd8.png', 'Попов А.А., индивидуальный предприниматель', '<p>Хочу поблагодарить сотрудников компании &nbsp;за хорошую, оперативную работу и замечательные человеческие качества! Брали в аренду два самосвала &ndash; МАН 41.480. Машины в хорошем состоянии. После аренды вернули назад, но забыли в салоне папку с документами. Папку вернули в тот же день. Большое за это спасибо!</p>\r\n'),
(3, 'ca35b3269ba3ff9ecc4e24c8ae40.png', 'Терехов А.Н., менеджер ОАО «ГрадСтрой»', '<p>Сотрудничаем &nbsp;уже очень давно, и приятно отметить, что они ни разу нас не подводили. Конечно, все познается в сравнении. Обращались мы и к некоторым другим поставщикам, но опыт этот был не слишком удачным: задержки в поставках, пересортица, и все при аналогичных расценках. Перестали экспериментировать, продолжаем сейчас по-прежнему работать с этой компанией и вполне довольны нашим сотрудничеством!</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(16) NOT NULL,
  `password` varchar(64) NOT NULL,
  `date_create` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `date_create`) VALUES
(1, 'admin', '$2y$13$TGTGbBh9WSgNOf33m0cx.ORn4modijlhgrsPlfpJm5j3argPqAH5G', 10000);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
