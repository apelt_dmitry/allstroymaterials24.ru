$(document).ready(function(){
    
        $('#carousel').carouFredSel({
            items: {
                visible: 2
            },
            width: '100%',
            auto: {
                play: true,
                duration: 1000
            },
            scroll: {
                duration: 750,
                items:1
            },
            prev: {
                button: '#prev',
                items: 1
            },
            next: {
                button: '#next',
                items: 1
            }
        });
        
        $('#carousel_glav').carouFredSel({
            items: {
                visible: 3
            },
            width: '100%',
            auto: {
                play: true,
                duration: 1000
            },
            scroll: {
                duration: 750,
                items:1
            },
            prev: {
                button: '#prev_glav',
                items: 1
            },
            next: {
                button: '#next_glav',
                items: 1
            }
        });
        
        $('#carousel_sand').carouFredSel({
            items: {
                visible: 4
            },
            width: '100%',
            auto: {
                play: true,
                duration: 1000
            },
            scroll: {
                duration: 750,
                items:1
            },
            prev: {
                button: '#prev_sand',
                items: 1
            },
            next: {
                button: '#next_sand',
                items: 1
            }
        });
        
        
        $('.secret').mouseover(function() {
            $(this).next().next().removeClass('unrotated').addClass('rotated');
        });
        $('.secret').mouseout(function() {
            $(this).next().next().removeClass('rotated').addClass('unrotated');
        });
});

